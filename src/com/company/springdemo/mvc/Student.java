package com.company.springdemo.mvc;

import validation.CourseCode;

import javax.validation.constraints.*;

public class Student {

    @NotNull(message="is required")
    @Size(min=1, message="is required")
    private String firstName;

    @NotNull(message="is required")
    @Size(min=1, message="is required")
    private String lastName;

    private String country;

    @Pattern(regexp = "[a-zA-Z0-9]{5}", message = "only 5 digits/characters")
    private String postalCode;

    @NotNull(message="is required")
    @Min(value=1, message="must be greater than 0")
    @Max(value= 10, message = "must be greater or equal to 10")
    private Integer semester;

    @CourseCode(value = "PME", message = "must start with PME")
    private String courseCode;

    public Student() {

    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Integer getSemester() {
        return semester;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
