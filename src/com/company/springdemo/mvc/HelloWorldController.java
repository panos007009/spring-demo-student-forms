package com.company.springdemo.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Locale;

@Controller
@RequestMapping("/hello")
public class HelloWorldController {

    @RequestMapping("/showForm")
    public String showForm() {
        return "helloworld-form";
    }

    @RequestMapping("/processForm")
    public String processFormDude(
            @RequestParam("studentName") String theName, Model model) {
        theName=theName.toUpperCase(Locale.ROOT);
        String result = "Hey there! " + theName;
        model.addAttribute("message",result);

        return "helloworld";
    }
}
