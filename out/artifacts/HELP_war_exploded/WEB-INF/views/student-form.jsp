<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>

<html>

<head>

    <title>Student registration Form</title>

    <style>
        .error {color : red}
    </style>

</head>


<body>

<i>Fill the  form of the student to be added. Asterisk (*) means the field is required.<i>

    <br><br>

<form:form action="processForm" modelAttribute="student">

    First Name (*):
    <form:input path="firstName"/>
    <form:errors path="firstName" cssClass="error"/>

    <br><br>

    Last Name (*):
    <form:input path="lastName"/>
    <form:errors path="lastName" cssClass="error"/>

    <br><br>

    Country:
    <form:select path="country">

        <form:options items="${theCountryOptions}"/>

    </form:select>

    <br><br>

    Semester:
    <form:input path="semester"/>
    <form:errors path="semester" cssClass="error"/>

    <br><br>

    Postal Code:
        <form:input path="postalCode"/>
        <form:errors path="postalCode" cssClass="error"/>

    <br><br>

    Course Code:
        <form:input path="courseCode"/>
        <form:errors path="courseCode" cssClass="error"/>

    <br><br>

    <input type="submit" value="Submit">

</form:form>

</body>

</html>