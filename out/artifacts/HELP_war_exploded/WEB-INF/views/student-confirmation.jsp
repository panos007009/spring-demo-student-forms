<!DOCTYPE html>

<html>

<head>

    <title>Student Confirmation</title>

</head>

<body>

The student is confirmed : ${student.firstName} ${student.lastName}

<br><br>

Country: ${student.country}

<br><br>

Semester: ${student.semester}

<br><br>

Postal Code: ${student.postalCode}

<br><br>

Course Code: ${student.courseCode}

</body>

</html>